public enum Square {
    X,
    O,
    BLANK;

    @Override
    public String toString(){
        String positionVal = "";
        if(this == Square.X){
            positionVal = "X";
        }
        if(this == Square.O){
            positionVal = "O";
        }
        if(this == Square.BLANK){
            positionVal =  "_";
        }
        return positionVal;
    }
}