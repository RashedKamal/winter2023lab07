import java.util.Scanner;
public class TicTacToeGame {
    public static void main(String[] args){
        Scanner s = new Scanner(System.in);
        System.out.println("Welcome to Tic Tac Toe!");
        Board board = new Board();
        boolean gameOver = false;
        int player = 1;
        Square playerToken = Square.X;
        while(!gameOver){
            System.out.println(board);
            if(player == 1){
                playerToken = Square.X;
            }
            else{
                playerToken = Square.O;
            }
            System.out.println("Player " + player + " Enter the row where you want to place your token: ");
            int row = s.nextInt();
            System.out.println("Player " + player + " Enter the column where you want to place your token: ");
            int col = s.nextInt();
            while(!board.placeToken(row, col, playerToken)){
                System.out.println("Input out of bounds! Please try again.");
                row = s.nextInt();
                col = s.nextInt();
            }
            if(board.checkIfFull()){
                System.out.println(board);
                System.out.println("It's a tie!");
                gameOver = true;
            }
            else if(board.checkIfWinning(playerToken)){
                System.out.println(board);
                System.out.println("Player " + player + " is the winner!");
                gameOver = true;
            }
            else{
                player++;
                if(player > 2){
                    player = 1;
                }
            }
        }
    }
}