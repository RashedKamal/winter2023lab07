public class Board {
    private Square[][] tictactoeBoard;

    public Board(){
        this.tictactoeBoard = new Square[3][3];
        for(int i=0; i<this.tictactoeBoard.length; i++){
            for(int j=0; j<this.tictactoeBoard[i].length; j++){
                this.tictactoeBoard[i][j] = Square.BLANK;
            }
        }
    }

    @Override
    public String toString(){
        String table = "";
        for(int i=0; i<this.tictactoeBoard.length; i++){
            table += "\n" + i + " ";
            for(int j=0; j<this.tictactoeBoard[i].length; j++){
                table+= this.tictactoeBoard[i][j] + " ";
            }
        }
        return table;
    }

    public boolean placeToken (int row, int col, Square playerToken){
        if((row < 0 || row > 2) || (col < 0 || col > 2)){
            return false;
        }
        if(this.tictactoeBoard[row][col] == Square.BLANK){
            this.tictactoeBoard[row][col] = playerToken;
            return true;
        }
        else{
            return false;
        }
    }

    public boolean checkIfFull(){
        for(Square[] token : this.tictactoeBoard){
            for(int i=0; i<this.tictactoeBoard.length; i++){
                if(token[i] == Square.BLANK){
                    return false;
                }

            }
        }
    return true;
    }

    private boolean checkIfWinningHorizontal(Square playerToken){
        int toWin = 0; // will increment to 3 if the player wins by horizontal
        for(int i=0; i<this.tictactoeBoard.length; i++, toWin = 0){
            for(int j=0; j<this.tictactoeBoard[i].length; j++){
                if(this.tictactoeBoard[i][j] == playerToken){
                    toWin += 1;
                }
                if(toWin == 3){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkIfWinningVertical(Square playerToken){
        int toWin = 0; // will increment to 3 if the player wins by vertical
        for(int i=0; i<this.tictactoeBoard.length; i++){
            for(int j=0; j<this.tictactoeBoard[i].length; j++, toWin =0){
                if(this.tictactoeBoard[i][j] == playerToken){
                    toWin += 1;
                }
                if(toWin == 3){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkIfWinningDiagonal(Square playerToken){
        int toWin = 0; // will increment to 3 if the player wins by diagonal
        for(int d=0; d<this.tictactoeBoard.length; d++){
            if(this.tictactoeBoard[d][d] == playerToken){
                toWin += 1;
            }
            if(toWin == 3){
                return true;
            }
        }
        return false;
    }

    public boolean checkIfWinning(Square playerToken){
        if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken)){
            return true;
        }
        else{
            return false;
        }
    }
}